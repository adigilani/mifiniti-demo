DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Cards;
CREATE TABLE Users(USER_ID INT PRIMARY KEY,
                  NAME VARCHAR(255) NOT NULL,
                  USERNAME VARCHAR(255) NOT NULL,
                  PASSWORD VARCHAR(255) NOT NULL,
                  IS_ADMIN BOOL);

insert into Users values(10001,'Administrator', 'admin', '@A9upo0a8', true);

CREATE TABLE Cards(CARD_ID INT PRIMARY KEY,
                  NUMBER VARCHAR(20) NOT NULL,
                  EXPIRY VARCHAR(5) NOT NULL,
                  CVV VARCHAR(3) NOT NULL,
                  USER INT,
                  foreign key (USER) references Users(USER_ID)
);