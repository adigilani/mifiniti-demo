# Mifiniti Demo Project!

This is a project to demo the test requirement as laid out in the correspondance.

## Backend

The backend folder contains a MAVEN Spring-Boot project. Run it as follow

1.  "mvn spring-boot:run".

The project uses some of the concept from Spring Boot to create a ReST API. For light authentication i have used JWT.

## Database

Project uses H2 as data store.

### Dummy Data
username: admin
passwrod: @A9upo0a8

## ReST API

There is a Postman collection to test the Rest API in the repo.
Also API endpoints can be seen at swagger ui. The address after running the backend application will be http://localhost:8080/swagger-ui.html#/

## Frontend

The frontend app is written using React and React Context. Run it

1.  npm install / yarn
2.  npm run start

There is a folder with screenshots of the Frontend App.

## Some credit card numbers to test

MasterCard 5555555555554444
MasterCard 5105105105105100
Visa 4111111111111111
Visa 4012888888881881

## A good strong password

@jLC08n6!
