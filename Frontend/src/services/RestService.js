import axios from 'axios';

class RestService {
  API_URL = 'http://localhost:8080/api/v1/';

  signinRequest = (username, password) => {
    return axios.post(`${this.API_URL}signin`, {
      username,
      password
    });
  };

  signupRequest = (name, username, password) => {
    return axios.post(`${this.API_URL}registration`, {
      name,
      username,
      password
    });
  };

  getCards = authToken => {
    return axios.get(`${this.API_URL}cards/list`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${authToken}`
      }
    });
  };

  searchCards = (searchTerm, authToken) => {
    return axios.get(`${this.API_URL}cards/search/${searchTerm}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${authToken}`
      }
    });
  };

  saveCard = (number, expiry, cvv, authToken) => {
    return axios.post(
      `${this.API_URL}cards/upsert`,
      {
        number,
        expiry,
        cvv
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${authToken}`
        }
      }
    );
  };
}

export default RestService;
