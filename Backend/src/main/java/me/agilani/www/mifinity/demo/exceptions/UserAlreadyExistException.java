package me.agilani.www.mifinity.demo.exceptions;

public class UserAlreadyExistException extends RuntimeException {
    public UserAlreadyExistException(String username) {
        super("A user with username (" + username + ") already exist.");
    }
}