package me.agilani.www.mifinity.demo.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

/**
 * This class is responsible for validating the expiry date of the card
 */
public class CreditCardExpiryConstraintValidator implements ConstraintValidator<ValidCreditCardExpiry, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if(s.isEmpty() || !s.contains("/") || s.length() >=6 ) {
                return false;
            }
            DateTimeFormatter f = DateTimeFormatter.ofPattern( "yy/MM" );
            YearMonth providedDate = YearMonth.parse(s, f);
            YearMonth now = YearMonth.now();
            return now.compareTo(providedDate) <= 0;
        } catch (Exception e) {
            return false;
        }
    }
}
