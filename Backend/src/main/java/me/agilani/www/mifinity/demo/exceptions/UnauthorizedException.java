package me.agilani.www.mifinity.demo.exceptions;

public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException (String e) {
        super("Not Authorized");
    }
}