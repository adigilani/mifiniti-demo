import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, FormGroup, FormControl, FormLabel } from 'react-bootstrap';
import './Signup.css';

export default class SignupPresentationalComponent extends Component {
  PropTypes = {
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    formValid: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired
  };

  handleSubmit = event => {
    const { handleSubmit } = this.props;
    handleSubmit(event);
  };

  handleChange = event => {
    const { handleChange } = this.props;
    handleChange(event);
  };

  render() {
    const { name, username, password, formValid } = this.props;
    return (
      <div className="signup">
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <h3 className="banner">Sign Up</h3>
          </FormGroup>
          <FormGroup controlId="name" bsSize="large">
            <FormLabel>Name</FormLabel>
            <FormControl
              autoFocus
              type="text"
              value={name}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="username" bsSize="large">
            <FormLabel>Username</FormLabel>
            <FormControl
              autoFocus
              type="text"
              value={username}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password</FormLabel>
            <FormControl
              value={password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button block bsSize="large" disabled={!formValid} type="submit">
            Sign up
          </Button>
        </form>
      </div>
    );
  }
}
