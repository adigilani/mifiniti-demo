package me.agilani.www.mifinity.demo.exceptions;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ControllerAdvice
@Slf4j
public class RestApplicationExceptionAdvice {

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<String> handleRunTimeException(RuntimeException e) {
        return error(INTERNAL_SERVER_ERROR, e.getMessage());
    }

    @ExceptionHandler({UnauthorizedException.class})
    public ResponseEntity<String> handleUnauthorizedException(UnauthorizedException e) {
        return error(UNAUTHORIZED, e.getMessage());
    }

    @ExceptionHandler({DataNotFoundException.class})
    public ResponseEntity<String> handleCreditCardNotFoundException(DataNotFoundException e) {
        return error(NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<String> handleCreditCardNotFoundException(UserNotFoundException e) {
        return error(NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler({UserAlreadyExistException.class})
    public ResponseEntity<String> handleUserRegistrationException(UserAlreadyExistException e){
        return error(CONFLICT, e.getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    protected ResponseEntity<String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

        Map<String, Object> body = new LinkedHashMap<>();
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        Gson gson = new Gson();
        String allErrors = gson.toJson(body);

        return error(BAD_REQUEST, allErrors);
    }

    private ResponseEntity<String> error(HttpStatus status, String e) {
        log.error("Exception: ", e);
        return ResponseEntity.status(status).body(e);
    }
}