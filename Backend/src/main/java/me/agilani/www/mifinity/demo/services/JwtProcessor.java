package me.agilani.www.mifinity.demo.services;

import lombok.extern.slf4j.Slf4j;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import java.util.Date;

import me.agilani.www.mifinity.demo.entities.User;
import me.agilani.www.mifinity.demo.exceptions.UnauthorizedException;
import org.springframework.stereotype.Service;

/**
 *  This class deals with JWT token creation and parsing
 */
@Service
@Slf4j
public class JwtProcessor implements BaseService {

    JwtProcessor() {}

    // The security token for the JWT
    private final  String SECRET_KEY = "IAmASecretKey";

    /**
     * This method creates a JWT token using the username and admin status
     * @param user
     * @return String a jwt token string
     */
    String createJWT(User user) {
        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(now.toString())
                .setIssuedAt(now)
                .setSubject(user.toString())
                .setIssuer("Adi")
                .signWith(signatureAlgorithm, signingKey);

        long expMillis = nowMillis + 18000000;
        Date exp = new Date(expMillis);
        builder.setExpiration(exp);

        return builder.compact();
    }

    /**
     * This method parse the JWT token to extract the subject of token
     * @param jwt String of JWT token to parse
     * @return String subject of token
     * @throws UnauthorizedException
     */
    String parseJWT(String jwt) throws UnauthorizedException {
        try {
            String token = jwt.replace("Bearer ", "");
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                    .parseClaimsJws(token).getBody();
            return claims.getSubject();
        } catch (Exception e) {
            throw new UnauthorizedException(e.getMessage());
        }
    }
}
