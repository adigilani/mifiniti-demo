package me.agilani.www.mifinity.demo.controllers;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import me.agilani.www.mifinity.demo.entities.CreditCard;
import me.agilani.www.mifinity.demo.exceptions.DataNotFoundException;
import me.agilani.www.mifinity.demo.exceptions.UnauthorizedException;
import me.agilani.www.mifinity.demo.services.CreditCardService;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * This class is a controller for credit cards and exposes following two rest api paths
 *  /search - accepts a query string param as search term
 *  /upsert - accepts a well formed credit card object and a string with jwt token
 */
@RestController
@RequestMapping("/api/v1/cards")
@Slf4j
@Api(value="Mifinity Backend Service", description="APIs for User Sign up and Signin")
public class CreditCardController {

    private CreditCardService ccService;

    CreditCardController(CreditCardService ccService) {
        this.ccService = ccService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/list")
    @ApiOperation(value = "Get all the cards data for a given user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List<CreditCard>"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 404, message = "Notfound request")
    })
    public List<CreditCard> getAll(
            @RequestHeader("Authorization") String jwt
    ) throws UnauthorizedException, DataNotFoundException {
        return this.ccService.getAllCreditCardsForUser(jwt);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/search/{term}")
    @ApiOperation(value = "Searches for credit cards using a search term", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List<CreditCard>"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 404, message = "Notfound request")
    })
    public List<CreditCard> search(
            @ApiParam(value = "Credit Card Number to Search", required = true)
            @PathVariable(value = "term") String searchTerm,
            @RequestHeader("Authorization") String jwt
    ) throws UnauthorizedException, DataNotFoundException {
        return this.ccService.searchCreditCardsForTermAndUser(searchTerm, jwt);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/upsert")
    @ApiOperation(value = "Insert or update a credit card data", response = CreditCard.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "CreditCard"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 404, message = "Notfound request")
    })
    public CreditCard upsert(
            @ApiParam(value = "Credit Card", required = true)
            @RequestBody CreditCard card,
            @RequestHeader("Authorization") String jwt
    ) throws UnauthorizedException {
        return this.ccService.upsertCC(card, jwt);
    }
}
