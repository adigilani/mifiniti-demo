import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AppContext from './AppContext';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <AppContext.Consumer>
    {({ authToken }) => (
      <Route
        render={props =>
          authToken !== undefined ? (
            <Component {...props} />
          ) : (
            <Redirect to="/" />
          )
        }
        {...rest}
      />
    )}
  </AppContext.Consumer>
);

export default PrivateRoute;
