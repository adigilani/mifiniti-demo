import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AppUtils from '../components/AppUtils';

import Signin from '../components/Signin';
import Signup from '../components/Signup';
import Dashboard from '../components/Dashboard';

export default (
  <Switch>
    <Route exact path="/" component={Signin} />
    <Route path="/signin" component={Signin} />
    <Route path="/signup" component={Signup} />
    <AppUtils.PrivateRoute path="/dashboard" component={Dashboard} />
  </Switch>
);
