import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import routes from './routes/routes';

import AppUtils from './components/AppUtils';

function App() {
  return (
    <AppUtils.AppContextProvider>
      <Router>{routes}</Router>
      <ToastContainer />
    </AppUtils.AppContextProvider>
  );
}

export default App;
