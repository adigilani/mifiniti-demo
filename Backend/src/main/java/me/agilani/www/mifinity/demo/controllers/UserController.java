package me.agilani.www.mifinity.demo.controllers;

import io.swagger.annotations.*;
import me.agilani.www.mifinity.demo.exceptions.UserAlreadyExistException;
import me.agilani.www.mifinity.demo.services.UserService;
import org.springframework.web.bind.annotation.*;
import me.agilani.www.mifinity.demo.entities.User;

import javax.validation.Valid;

/**
 * This class is a controller for users and exposes following two rest api paths
 *  /registration - accepts a well formed User object
 *  /signin - accepts an object with username and password
 */
@RestController
@RequestMapping("/api/v1")
@Api(value="Mifinity Backend Service", description="APIs for User Sign up and Signin")
public class UserController {

    private UserService userService;

    public UserController(
            UserService userService
    ) {
        this.userService = userService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/registration")
    @ApiOperation(value = "Create a new user", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JWT Token"),
            @ApiResponse(code = 400, message = "List of errors")
    })
    public String createNewUser(
            @ApiParam(value = "User object", required = true)
            @Valid @RequestBody User newUser
    ) throws UserAlreadyExistException {
        return this.userService.createNewUser(newUser);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/signin")
    @ApiOperation(value = "Authenticate a user", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JWT Token"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public String signin(
            @ApiParam(value = "User object", required = true)
            @Valid @RequestBody User user
    ) throws UserAlreadyExistException {
        return this.userService.authenticateUser(user);
    }
}
