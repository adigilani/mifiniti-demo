import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CreditCardInput from 'react-credit-card-input';
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  Card,
  InputGroup,
  FormControl
} from 'react-bootstrap';

import './Dashboard.css';

class DashboardPresentationComponent extends PureComponent {
  PropTypes = {
    data: PropTypes.array.isRequired,
    newCard: PropTypes.object.isRequired,
    isEdit: PropTypes.bool.isRequired,
    handleCardCVCChange: PropTypes.func.isRequired,
    handleCardExpiryChange: PropTypes.func.isRequired,
    handleCardNumberChange: PropTypes.func.isRequired,
    handleNewCardSave: PropTypes.func.isRequired,
    handleEdit: PropTypes.func.isRequired,
    handleSearchTermChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    handleCancelSearch: PropTypes.func.isRequired
  };

  handleNewCardSave = event => {
    const { handleNewCardSave } = this.props;
    handleNewCardSave(event);
  };

  handleCardNumberChange = event => {
    const { handleCardNumberChange } = this.props;
    handleCardNumberChange(event);
  };

  handleCardExpiryChange = event => {
    const { handleCardExpiryChange } = this.props;
    handleCardExpiryChange(event);
  };

  handleCardCVCChange = event => {
    const { handleCardCVCChange } = this.props;
    handleCardCVCChange(event);
  };

  onEditClick = event => {
    const { handleEdit } = this.props;
    handleEdit(event);
  };

  handleSearch = event => {
    const { handleSearch } = this.props;
    handleSearch(event);
  };

  handleSearchTermChange = event => {
    const { handleSearchTermChange } = this.props;
    handleSearchTermChange(event);
  };

  handleCancelSearch = event => {
    const { handleCancelSearch } = this.props;
    handleCancelSearch(event);
  };

  getTableRows = () => {
    const { data } = this.props;
    return data.map(cc => (
      <tr>
        <td>{cc.card_id}</td>
        <td>{cc.number}</td>
        <td>{cc.expiry}</td>
        <td>{cc.cvv}</td>
        <td>
          <Button variant="primary" onClick={() => this.onEditClick(cc)}>
            Edit
          </Button>
        </td>
      </tr>
    ));
  };

  render() {
    const { newCard, isEdit } = this.props;
    return (
      <Container>
        <Container>
          <Card>
            <Card.Header>
              {isEdit ? <div>Edit card</div> : <div>New card</div>}
            </Card.Header>
            <Card.Body>
              <Card.Title>
                {isEdit ? <div>Edit card</div> : <div>Add new card</div>}
              </Card.Title>
              <Card.Text>
                <Row>
                  <Col>
                    <CreditCardInput
                      cardNumberInputProps={{
                        value: newCard.number,
                        onChange: this.handleCardNumberChange
                      }}
                      cardExpiryInputProps={{
                        value: newCard.expiry,
                        onChange: this.handleCardExpiryChange
                      }}
                      cardCVCInputProps={{
                        value: newCard.cvv,
                        onChange: this.handleCardCVCChange
                      }}
                      fieldClassName="input"
                    />
                  </Col>
                  <Col>
                    <Button variant="primary" onClick={this.handleNewCardSave}>
                      Save
                    </Button>
                  </Col>
                </Row>
              </Card.Text>
            </Card.Body>
          </Card>
        </Container>
        <p></p>
        <Container>
          <Card>
            <Card.Header>Search</Card.Header>
            <Card.Body>
              <Card.Title>Search for a card</Card.Title>
              <Card.Text>
                <Row>
                  <Col>
                    <InputGroup size="sm" className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">
                          Search Term
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        onChange={this.handleSearchTermChange}
                        aria-label="Small"
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </InputGroup>
                  </Col>
                  <Col>
                    <Button variant="primary" onClick={this.handleSearch}>
                      Search
                    </Button>
                  </Col>
                  <Col>
                    <Button variant="primary" onClick={this.handleCancelSearch}>
                      Cancel
                    </Button>
                  </Col>
                </Row>
              </Card.Text>
            </Card.Body>
          </Card>
        </Container>
        <p></p>
        <Container>
          <Row>
            <Col>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Number</th>
                    <th>Expiry</th>
                    <th>CVV</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>{this.getTableRows()}</tbody>
              </Table>
            </Col>
          </Row>
        </Container>
      </Container>
    );
  }
}

export default DashboardPresentationComponent;
