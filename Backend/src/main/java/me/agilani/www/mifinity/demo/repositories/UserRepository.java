package me.agilani.www.mifinity.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import me.agilani.www.mifinity.demo.entities.User;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByUsername(String username);
    List<User> findByUsernameAndPassword(String username, String password);
}