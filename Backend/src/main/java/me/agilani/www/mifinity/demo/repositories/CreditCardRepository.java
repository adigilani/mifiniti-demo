package me.agilani.www.mifinity.demo.repositories;

import me.agilani.www.mifinity.demo.entities.CreditCard;
import me.agilani.www.mifinity.demo.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CreditCardRepository extends PagingAndSortingRepository<CreditCard, Integer> {

    List<CreditCard> findByNumberContainingAndUser(String number, User user);
    List<CreditCard> findByNumberContaining(String number);
    List<CreditCard> findByUser(User user);

    @Query("SELECT cards FROM CreditCard cards")
    List<CreditCard> findAll();
}
