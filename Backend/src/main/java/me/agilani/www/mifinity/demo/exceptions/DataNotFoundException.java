package me.agilani.www.mifinity.demo.exceptions;

public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(String ccNumber) {
        super("Could not find credit card " + ccNumber);
    }

    public DataNotFoundException(String username, String password) {
        super("User with username " + username + " not found!!!");
    }
}
