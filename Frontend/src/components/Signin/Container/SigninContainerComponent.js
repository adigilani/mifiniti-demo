import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import AppUtils from '../../AppUtils';
import SigninPresentationComponent from '../Presentation/SigninPresentationComponent';
import RestService from '../../../services';

class SigninContainer extends PureComponent {
  static contextType = AppUtils.AppContext;

  constructor() {
    super();
    this.restService = new RestService();
    this.state = {
      username: '',
      password: '',
      formValid: false
    };
  }

  componentDidMount() {}

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  validateForm = async () => {
    const { username, password } = this.state;
    const isValid = username.length > 0 && password.length > 0;
    await this.setStateAsync({
      formValid: isValid
    });
  };

  handleChange = async event => {
    await this.setStateAsync({
      [event.target.id]: event.target.value
    });
    this.validateForm();
  };

  handleSubmit = event => {
    event.preventDefault();
    const { formValid } = this.state;
    if (!formValid) {
      return false;
    }
    this.doSignin();
    return false;
  };

  doSignin = async () => {
    try {
      const { username, password } = this.state;
      const { signIn } = this.context;
      const response = await this.restService.signinRequest(username, password);
      signIn(response.data);
    } catch (e) {
      toast.error('Invalid username or password');
    }
  };

  render() {
    const { username, password, formValid } = this.state;
    const { authToken } = this.context;

    if (authToken !== undefined) {
      return <Redirect to="/dashboard" />;
    }

    return (
      <SigninPresentationComponent
        username={username}
        password={password}
        formValid={formValid}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default SigninContainer;
