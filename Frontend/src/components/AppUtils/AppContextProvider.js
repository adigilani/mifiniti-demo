/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';

import AppContext from './AppContext';

class AppContextProvider extends Component {
  state = {
    authToken: undefined
  };

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  render() {
    const { authToken } = this.state;
    const { children } = this.props;
    return (
      <AppContext.Provider
        value={{
          authToken,
          signIn: async jwtToken => {
            await this.setStateAsync({
              authToken: jwtToken
            });
          },
          signout: async () => {
            await this.setStateAsync({
              authToken: undefined
            });
          }
        }}
      >
        {children}
      </AppContext.Provider>
    );
  }
}

export default AppContextProvider;
