package me.agilani.www.mifinity.demo.services;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import me.agilani.www.mifinity.demo.entities.CreditCard;
import me.agilani.www.mifinity.demo.entities.User;
import me.agilani.www.mifinity.demo.exceptions.DataNotFoundException;
import me.agilani.www.mifinity.demo.exceptions.UnauthorizedException;
import me.agilani.www.mifinity.demo.repositories.CreditCardRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Credit card service
 */
@Service
@Slf4j
public class CreditCardService implements BaseService{

    private CreditCardRepository ccRepo;
    private JwtProcessor jwtProcessor;
    private UserService userService;

    CreditCardService(
            CreditCardRepository ccRepo,
            JwtProcessor jwtProcessor,
            UserService userService
    ){
        this.ccRepo = ccRepo;
        this.jwtProcessor = jwtProcessor;
        this.userService = userService;
    }

    /**
     * Search for the credit cards using the search term and user
     * @param creditCardSearchTerm - String with credit card term
     * @param jwtToken - String jwt token containing user and admin privileges
     * @return List - List of credit cards
     * @throws UnauthorizedException
     * @throws DataNotFoundException
     */
    public List<CreditCard> searchCreditCardsForTermAndUser(
            String creditCardSearchTerm,
            String jwtToken
    ) throws UnauthorizedException, DataNotFoundException {
        User user = this.getUserFromJWTToken(jwtToken);

        List<CreditCard> list;
        if (!user.isAdmin()) {
            list = this.ccRepo.findByNumberContainingAndUser(creditCardSearchTerm, user);
        } else {
            log.info("findByCreditCardNo " + user.toString());
            list = this.ccRepo.findByNumberContaining(creditCardSearchTerm);
        }
        list.forEach(cc -> cc.setUser(null));
        return list;
    }

    /**
     * Get all the credit cards using the user
     * @param jwtToken - String jwt token containing user and admin privileges
     * @return List - List of credit cards
     * @throws UnauthorizedException
     * @throws DataNotFoundException
     */
    public List<CreditCard> getAllCreditCardsForUser(
            String jwtToken
    ) throws UnauthorizedException, DataNotFoundException {
        User user = this.getUserFromJWTToken(jwtToken);

        List<CreditCard> list;
        if (!user.isAdmin()) {
            list = this.ccRepo.findByUser(user);
        } else {
            list = this.ccRepo.findAll();
        }
        list.forEach(cc -> cc.setUser(null));
        return list;
    }

    /**
     * @param card A well formed credit card object
     * @param jwtToken A string JWT token
     * @return Credit Card
     * @throws UnauthorizedException
     */
    public CreditCard upsertCC(
            CreditCard card,
            String jwtToken
    ) throws UnauthorizedException {
        User user = this.getUserFromJWTToken(jwtToken);
        List<CreditCard> cards = this.ccRepo.findByNumberContainingAndUser(card.getNumber(), user);
        CreditCard newCardToBeSaved;
        if (cards.size()>0) {
            newCardToBeSaved = cards.get(0);
            newCardToBeSaved.setExpiry(card.getExpiry());
        } else {
            newCardToBeSaved = card;
        }
        newCardToBeSaved.setUser(user);
        this.ccRepo.save(newCardToBeSaved);
        newCardToBeSaved.setUser(null);
        return newCardToBeSaved;
    }

    /**
     * Parse a jwt token and return a User
     * @param String jwtToken
     * @return User
     */
    private User getUserFromJWTToken(String jwtToken) {
        String jwtSubjects = this.jwtProcessor.parseJWT(jwtToken);
        Gson gson = new Gson();
        User tokenUser = gson.fromJson(jwtSubjects, User.class);
        User  actualUser = this.userService.findUser(tokenUser.getUsername());
        if (actualUser == null) {
            throw new UnauthorizedException("Unauthorized request");
        }
        return actualUser;
    }
}
