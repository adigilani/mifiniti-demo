import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import AppUtils from '../../AppUtils';
import SignupPresentationComponent from '../Presentation/SignupPresentationComponent';
import RestService from '../../../services';

class SignupContainer extends PureComponent {
  static contextType = AppUtils.AppContext;

  constructor() {
    super();
    this.restService = new RestService();
    this.state = {
      name: '',
      username: '',
      password: '',
      formValid: false
    };
  }

  componentDidMount() {}

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  validateForm = async () => {
    const { name, username, password } = this.state;
    const isValid =
      name.length > 0 && username.length > 0 && password.length > 0;
    await this.setStateAsync({
      formValid: isValid
    });
  };

  handleChange = async event => {
    await this.setStateAsync({
      [event.target.id]: event.target.value
    });
    this.validateForm();
  };

  handleSubmit = event => {
    event.preventDefault();
    const { formValid } = this.state;
    if (!formValid) {
      return false;
    }
    this.doSignup();
    return false;
  };

  doSignup = async () => {
    try {
      const { name, username, password } = this.state;
      const { signIn } = this.context;
      const response = await this.restService.signupRequest(
        name,
        username,
        password
      );
      toast.success('User created!!!');
      signIn(response.data);
    } catch (e) {
      toast.error(e.message);
    }
  };

  render() {
    const { name, username, password, formValid } = this.state;
    const { authToken } = this.context;

    if (authToken !== undefined) {
      return <Redirect to="/dashboard" />;
    }

    return (
      <SignupPresentationComponent
        name={name}
        username={username}
        password={password}
        formValid={formValid}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default SignupContainer;
