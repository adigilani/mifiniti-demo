package me.agilani.www.mifinity.demo.services;

import lombok.extern.slf4j.Slf4j;
import me.agilani.www.mifinity.demo.entities.User;
import me.agilani.www.mifinity.demo.exceptions.UserAlreadyExistException;
import me.agilani.www.mifinity.demo.exceptions.UserNotFoundException;
import me.agilani.www.mifinity.demo.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This service class deals with user creation and authentication
 */
@Service
@Slf4j
public class UserService implements BaseService {

    private final UserRepository userRepo;
    private JwtProcessor jwtProcessor;

    UserService(
            UserRepository userRepo,
            JwtProcessor jwtProcessor
    ) {
        this.userRepo = userRepo;
        this.jwtProcessor = jwtProcessor;
    }

    /** Creates a new user
     * @param newUser - The user
     * @return String - A JWT token
     * @throws UserAlreadyExistException
     */
    public String createNewUser(User newUser) throws UserAlreadyExistException {
        List<User> foundUsers = this.userRepo.findByUsername(newUser.getUsername());
        if (foundUsers.size() > 0 ) {
            throw new UserAlreadyExistException(newUser.getUsername());
        }

        // might want to encrypt user details eg username and password

        newUser.setAdmin(false);
        this.userRepo.save(newUser);
        return this.jwtProcessor.createJWT(newUser);
    }

    /**
     * user login with login name and password ad upon success a simple jwt is returned
     * @param user a user
     * @return String jwt token
     */
    public String authenticateUser(User user) {
        List<User> foundUsers = this.userRepo.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        if (foundUsers.size() <= 0 ) {
            throw new UserNotFoundException(user.getUsername());
        }
        return this.jwtProcessor.createJWT(foundUsers.get(0));
    }

    /**
     * finds a user using the username
     * @param String a username
     * @return User
     */
    public User findUser(String username) {
        List<User> foundUsers = this.userRepo.findByUsername(username);
        return foundUsers.get(0);
    }
}
