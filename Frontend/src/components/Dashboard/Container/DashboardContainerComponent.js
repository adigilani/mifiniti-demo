import React, { PureComponent } from 'react';
import { toast } from 'react-toastify';
import _ from 'lodash';

import AppUtils from '../../AppUtils';
import RestService from '../../../services';
import DashboardPresentationalComponent from '../presentation/DashboardPresentationalComponent';

class DashboardContainerComponent extends PureComponent {
  static contextType = AppUtils.AppContext;

  constructor(props) {
    super(props);
    this.restService = new RestService();
    this.state = {
      data: [],
      isEdit: false,
      newCard: {
        number: '',
        expiry: '',
        cvv: ''
      },
      searchTerm: ''
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  handleCardNumberChange = async event => {
    const { newCard } = this.state;
    await this.setStateAsync({
      newCard: {
        ...newCard,
        number: event.target.value
      }
    });
  };

  handleCardExpiryChange = async event => {
    const { newCard } = this.state;
    await this.setStateAsync({
      newCard: {
        ...newCard,
        expiry: event.target.value
      }
    });
  };

  handleCardCVCChange = async event => {
    const { newCard } = this.state;
    await this.setStateAsync({
      newCard: {
        ...newCard,
        cvv: event.target.value
      }
    });
  };

  handleEdit = async event => {
    await this.setStateAsync({
      isEdit: true,
      newCard: event
    });
  };

  handleNewCardSave = async () => {
    try {
      const { authToken } = this.context;
      const { newCard, data, isEdit } = this.state;
      const { number, expiry, cvv } = newCard;
      const expiryParts = expiry.split('/');
      const newExpiry = `${expiryParts[1]}/${expiryParts[0]}`.trim();
      const result = await this.restService.saveCard(
        number,
        newExpiry,
        cvv,
        authToken
      );
      if (isEdit) {
        _.remove(data, cc => cc.card_id === newCard.card_id);
      }
      data.push(result.data);
      await this.setStateAsync({
        data,
        isEdit: false,
        newCard: {
          number: '',
          expiry: '',
          cvv: ''
        }
      });
      toast.success('Card saved!!!');
    } catch (e) {
      console.log(e);
      toast.error('Card not saved!!!');
    }
  };

  handleSearchTermChange = async event => {
    await this.setStateAsync({
      searchTerm: event.target.value
    });
  };

  handleSearch = async () => {
    const { searchTerm } = this.state;
    const { authToken } = this.context;
    const response = await this.restService.searchCards(searchTerm, authToken);
    await this.setStateAsync({ data: response.data });
  };

  handleCancelSearch = async () => {
    await this.setStateAsync({
      searchTerm: ''
    });
    this.retrieveData();
  };

  retrieveData = async () => {
    const { authToken } = this.context;
    const response = await this.restService.getCards(authToken);
    await this.setStateAsync({ data: response.data });
  };

  render() {
    const { data, newCard, isEdit } = this.state;
    return (
      <DashboardPresentationalComponent
        data={data}
        newCard={newCard}
        isEdit={isEdit}
        handleCardNumberChange={this.handleCardNumberChange}
        handleCardExpiryChange={this.handleCardExpiryChange}
        handleCardCVCChange={this.handleCardCVCChange}
        handleNewCardSave={this.handleNewCardSave}
        handleEdit={this.handleEdit}
        handleSearchTermChange={this.handleSearchTermChange}
        handleSearch={this.handleSearch}
        handleCancelSearch={this.handleCancelSearch}
      ></DashboardPresentationalComponent>
    );
  }
}

export default DashboardContainerComponent;
